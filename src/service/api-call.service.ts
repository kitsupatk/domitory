import { Injectable } from '@angular/core';

import { takeUntil, first, take } from 'rxjs/operators';

import { HttpRouterService } from './_httpRouterService';

@Injectable({
  providedIn: 'root'
})
export class ApiCallService {

  constructor(
    private httpRouterService: HttpRouterService
  ) { }

  async test() {
    const [method, url] = ['GET', 'http://localhost:3600/api'];
    return await this.httpRouterService.call(method, url).pipe(first()).toPromise()
  }

  async getDormitoryList() {
    const [method, url] = ['GET', 'http://localhost:3600/api/dormitoryList'];
    return await this.httpRouterService.call(method, url).pipe(first()).toPromise()
  }
  
  async login(body: any) {
    const [method, url] = ['POST', 'http://localhost:3600/api/login'];
    return await this.httpRouterService.call(method, url, body).pipe(first()).toPromise()
  }
}
