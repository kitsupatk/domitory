import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class HttpRouterService {
  constructor(
    private http: HttpClient) {
  }

  call(method: string, url: string, body = {}): Observable<any> {
    const headerDirect = {
      'Content-Type': 'application/json',
    }

    const requestOptions = {
      headers: new HttpHeaders(headerDirect),
    };

    if (method === 'GET') {
      return this.http.get<any>(url, requestOptions);
    } else if (method === 'POST') {
      return this.http.post<any>(url, body, requestOptions).pipe(
        map(data => {
          return data;
        })
      );
    }
  }

}
