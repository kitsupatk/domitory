import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';

import { ApiCallService } from 'src/service/api-call.service';

import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  validateForm!: UntypedFormGroup;
  
  @Output() setAuth = new EventEmitter();

  constructor(
    private formBuilder: UntypedFormBuilder,
    private apiCallService: ApiCallService,
    private _toastrService: ToastrService,
    ) { }

  async ngOnInit() {
    this.validateForm = this.formBuilder.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }

  async submitForm() {
    if (this.validateForm.valid) {
      const formLogin = {
        username: this.validateForm.value.username,
        password: this.validateForm.value.password
      }
      await this.apiCallService.login(formLogin).then(respObj => {
        this._toastrService.success('Success', 'isAuth success');
        this.setAuth.emit(true);
      }).catch(err => {
        this._toastrService.error('Username or password not match', 'isAuth error');
        console.log('isAuth error');
      });
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }
}
