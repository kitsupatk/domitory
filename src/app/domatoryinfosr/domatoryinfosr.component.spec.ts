import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DomatoryinfosrComponent } from './domatoryinfosr.component';

describe('DomatoryinfosrComponent', () => {
  let component: DomatoryinfosrComponent;
  let fixture: ComponentFixture<DomatoryinfosrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DomatoryinfosrComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DomatoryinfosrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
