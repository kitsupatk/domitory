import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr'; // For auth after login toast

import { AgmCoreModule } from '@agm/core';

import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzRateModule } from 'ng-zorro-antd/rate';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzFormModule } from 'ng-zorro-antd/form';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { PopularComponent } from './popular/popular.component';
import { DomatoryinfoComponent } from './domatoryinfo/domatoryinfo.component';
import { SrComponent } from './sr/sr.component';
import { LoginComponent } from './login/login.component';

import { HttpRouterService } from 'src/service/_httpRouterService';
import { ApiCallService } from 'src/service/api-call.service';
import { DomatoryinfosrComponent } from './domatoryinfosr/domatoryinfosr.component';


@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    PopularComponent,
    DomatoryinfoComponent,
    SrComponent,
    LoginComponent,
    DomatoryinfosrComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot({ progressBar: true, maxOpened: 1, autoDismiss: true }), // ToastrModule added

    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAiUdXcsGPt7rpOqc1Onortw7RCowmhPlc'
    }),

    NzLayoutModule,
    NzBreadCrumbModule,
    NzMenuModule,
    NzButtonModule,
    NzGridModule,
    NzImageModule,
    NzCardModule,
    NzRateModule,
    NzAvatarModule,
    NzFormModule,
  ],
  providers: [HttpRouterService, ApiCallService],
  bootstrap: [AppComponent]
})
export class AppModule { }
