import { Component, OnInit } from '@angular/core';

import { ApiCallService } from 'src/service/api-call.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  isAuth = false;

  domatoryList: any = [];
  domantoryDetail: any = {};

  title = 'domatory';
  _link = '';
  _method = '';

  constructor(
    private apiCallService : ApiCallService
  ) {

  }

  setAuth(canActive: boolean) {
    this.isAuth = canActive;
  }

  async ngOnInit() {
    this.domatoryList = await this.apiCallService.getDormitoryList();
    console.log(this.domatoryList);
  }

}

