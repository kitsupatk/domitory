import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  inputValue?: string;
  options: string[] = [];

  markerLocation:any = [
    {
      lat: 13.778554698604601,
      lng: 100.56005718825766,
      desc: 'หอหลุยแมนชั่น'
      href='/https://goo.gl/maps/3BK6gELS8pHozcjQ8' ,
    },
    {
      lat: 13.777955794269221,
      lng: 100.56177101114703,
      desc: 'หออรแมนชั่น'
      href="https://goo.gl/maps/AuwmG7SSR7e58Lh39",
    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

  onInput(event: Event): void {
    const value = (event.target as HTMLInputElement).value;
    this.options = value ? [value, value + value, value + value + value] : [];
  }

  onMouseOver(infoWindow: any, gm: any) {
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;
    infoWindow.open();
  }
}