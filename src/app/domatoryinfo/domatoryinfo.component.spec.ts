import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DomatoryinfoComponent } from './domatoryinfo.component';

describe('DomatoryinfoComponent', () => {
  let component: DomatoryinfoComponent;
  let fixture: ComponentFixture<DomatoryinfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DomatoryinfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DomatoryinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
